 /* Arbeitsauftrag:  Lesen Sie den Titel und den Autor des Buches "Java ist auch eine Insel" 
 *					 aus der Datei "buchhandlung.xml" und geben Sie sie auf dem Bildschirm aus.
 *
 *                   Ausgabe soll wie folgt aussehen:
 *                        titel:  Java ist auch eine Insel 
 *                        autor:  Max Mustermann 
 */

import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

public class ReadBookstoreData2 {

	public static void main(String[] args) {

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

		try {
			//Hier wird die XML aufgerufen
			DocumentBuilder dbuilder = factory.newDocumentBuilder();
			Document doc = dbuilder.parse("buchhandlung.xml");

			//Hier wird der Titel Aufgerufen
			Node title = doc.getElementsByTagName("titel").item(0); 
			Element titleElem = (Element)title; 
			System.out.println(titleElem.getNodeName() + ": " + titleElem.getTextContent());
			
			//Hier wird der Autor Aufgerufen
			Node autor = doc.getElementsByTagName("autor").item(0); 
			Element autorElem = (Element)autor; 
			System.out.println(autorElem.getNodeName() + ": " + autorElem.getTextContent());
			
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
