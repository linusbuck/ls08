import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * 
 */

/**
 * @author Linus Buck
 * @Date 12.08.2021
 * @Version 0.1
 */
public class CSVDatenLesen {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		try {
			BufferedReader reader = new BufferedReader (new FileReader("artikelliste.csv"));
			String zeile = "";
			
			while ( (zeile = reader.readLine()) != null) {
				System.out.println(zeile);
			}
				reader.close();
			
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	}

}
